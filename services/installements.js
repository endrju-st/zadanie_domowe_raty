const installementService = angular.module('installementService', [])

installementService.service('InstallementService', function ($rootScope, $q) {

    this.calculateInstallement = (loan) => {
        let restOfLoan = loan.amount
        let installement = 0
        let capitalPart = (loan.amount / loan.months)
        let listOfInstallements = []
        let restOfAmmout = loan.amount
        for (let index = 0; index < loan.months; index++) {
            installement = capitalPart +
                (restOfLoan * loan.interest) / 100 / loan.months;
            restOfLoan = restOfLoan - capitalPart;
            restOfAmmout = restOfAmmout - installement
            listOfInstallements[index] = {
              installement: installement.toFixed(2),
              restOfAmmout: restOfAmmout.toFixed(2)
            };
        }
        return listOfInstallements;
    }

    this.sendCalculationToDb = (listOfInstallements) => {
        return $q.resolve(fetch('http://localhost:3000/installement/', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(listOfInstallements),
        }).then(resp => resp.json()))
      }
}
)